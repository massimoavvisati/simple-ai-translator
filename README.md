# Simple AI Translator using Transformers.js and Web Workers API

This is a simple web application that utilizes the power of AI for translation tasks, powered by [Transformers.js](https://github.com/xenova/transformers.js) library by [Xenova](https://twitter.com/xenovacom) and [Web Workers API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API).

With this application, users can translate text from Italian to English directly in their web browser without the need for a backend server.

## Advantages of Client-side AI

Using AI models directly in the web browser without relying on a backend server offers several advantages:

- **Privacy and Security**: User data remains on the client-side, enhancing privacy and security.

- **Scalability**: No need to manage server infrastructure, making it easy to scale the application.

- **Offline Capability**: The application can function even without an internet connection, ensuring uninterrupted user experience.

## How it Works

The application consists of three main components:

1. **index.html**: Defines the structure of the web page, including input and output text areas, a translation button, and a progress bar.

2. **worker.js**: Web Worker script responsible for handling the translation task. It downloads the specified AI model and performs the translation asynchronously in a separate thread.

3. **app.js**: Manages the user interaction with the application. It initializes the Web Worker, sends text to be translated, and updates the UI based on the translation progress and result.

## Getting Started

To run this project, simply serve the `public/` folder with any web server.

For example you may install and run `http-server`:

```bash
npm install -g http-server
http-server ./public/
```


For more information on Transformers.js and its capabilities, please visit the [official documentation](https://huggingface.co/docs/transformers.js).

## Contributing

Contributions are welcome! If you have any suggestions or improvements for the application, feel free to open an issue or submit a pull request.

## License

Made with ❤️ by [Massimo Avvisati](https://twitter.com/MassimoAvvisati)

This project is licensed under the [GPL v3 License](https://www.gnu.org/licenses/gpl-3.0.html) - see the [LICENSE](https://www.gnu.org/licenses/gpl-3.0.html#license-text) file for details.
