document.addEventListener("DOMContentLoaded", () => {

    const translateButton = document.getElementById('translate-button');
    const italianTextarea = document.getElementById('italian-text');
    const englishTextarea = document.getElementById('english-text');
    const progressBar = document.getElementById('progress-bar');

    const disableUI = () => {
        italianTextarea.setAttribute('disabled', true);
        englishTextarea.setAttribute('disabled', true);
        translateButton.setAttribute('disabled', true);
        translateButton.innerText = 'Translating...'
    }

    const enableUI = () => {
        italianTextarea.removeAttribute('disabled');
        englishTextarea.removeAttribute('disabled');
        translateButton.removeAttribute('disabled');
        translateButton.innerText = 'Translate'
    }


    var aiWorker = new Worker('worker.js', {
        type: "module"
    });

    const translate = (text) => {
        aiWorker.postMessage({
            action: 'translate',
            input: text
        })
    }
    const download = (model) => {
        aiWorker.postMessage({
            action: 'download',
            task: 'translation',
            model: model
        });
    }

    aiWorker.addEventListener('message', (event) => {
        const { status, result } = event.data;

        if (status == 'downloading') {
            if (result.status == 'progress') {
                progressBar.style.setProperty("--progress-bar-percentage", result.progress + "%");
            }
        } else if (status == 'update') {
            englishTextarea.value = result;
        } else if (status == 'result') {
            englishTextarea.value = result;
            enableUI();
            
        } else if (status == 'ready') {
            translateButton.addEventListener('click', () => {
                disableUI();
                translate(italianTextarea.value);
            })
            enableUI();
        }
    })

    download('Xenova/opus-mt-it-en');

})